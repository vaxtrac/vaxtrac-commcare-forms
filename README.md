**CommCareHQ Application Form Designs**

How to import an application design from a json export: https://confluence.dimagi.com/display/commcarepublic/Copying+an+Application+between+Projects+or+Servers

The CommCare Zip files (.ccz is a regular zip file) can be installed using the offline option in the CommCare ODK instead of the barcode or tiny url.